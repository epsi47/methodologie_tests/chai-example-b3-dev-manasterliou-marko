const Cube = require('../src/app').Cube;
const Triangle = require('../src/app').Triangle;
const Cercle = require('../src/app').Cercle;
const expect = require('chai').expect;

describe('Testing the Cube Functions', function() {
    it('1. The side length of the Cube', function(done) {
        let c1 = new Cube(2);
        expect(c1.getSideLength()).to.equal(2);
        done();
    });
    
    it('2. The surface area of the Cube', function(done) {
        let c2 = new Cube(5);
        expect(c2.getSurfaceArea()).to.equal(150);
        done();
    });
    
    it('3. The volume of the Cube', function(done) {
        let c3 = new Cube(7);
        expect(c3.getVolume()).to.equal(343);
        done();
    });
    
});

describe('Testing the Triangle Functions', function() {
    it('1. La longueur du côté AB du triangle', function(done) {
        let t1 = new Triangle(5, 6, 10);
        expect(t1.getABLength()).to.equal(5);
        done();
    })
    it('2. La longueur du côté AC du triangle', function(done) {
        let t1 = new Triangle(5, 6, 10);
        expect(t1.getACLength()).to.equal(6);
        done();
    })
    it('3. La longueur du côté BC du triangle', function(done) {
        let t1 = new Triangle(5, 6, 10);
        expect(t1.getBCLength()).to.equal(10);
        done();
    })
    it('4. Ne construire un triangle avec que des nombres positifs', function(done) {
        let t1 = new Triangle(-1, -8, -5);
        expect(typeof t1.getABLength()).to.equal("undefined");
        let t2 = new Triangle(-1, 2, 5);
        expect(typeof t2.getABLength()).to.equal("undefined");
        let t3 = new Triangle(-1, 0, 6);
        expect(typeof t3.getABLength()).to.equal("undefined");
        let t4 = new Triangle(0, 0, 0);
        expect(typeof t4.getABLength()).to.equal("undefined");
        let t5 = new Triangle(5, 0, 10);
        expect(typeof t5.getABLength()).to.equal("undefined");
        let t6 = new Triangle("2", "5", "6");
        expect(typeof t6.getABLength()).to.equal("undefined");
        done();
    })
    it('5. Ne construire un triangle que si la longueur du plus grand côté est supérieure à la somme des deux autres',
        function(done) {
        let t1 = new Triangle(5, 6, 12);
        expect(typeof t1.getABLength()).to.equal("undefined");
        let t2 = new Triangle(5, 6, 11);
        expect(typeof t2.getABLength()).to.equal("undefined");
        done();
    })
    it('6. Perimètre d\'un triangle', function(done) {
        let t1 = new Triangle(5, 6, 10)
        expect(t1.getPerimetre()).to.equal(21)
        done();
    })
    it('7. Aire d\'un triangle sans la hauteur', function(done) {
        let t1 = new Triangle(5, 6, 10)
        expect(t1.getSurface()).to.equal(11.4)
        done();
    })
    it('8. Triangle équilatéral', function(done) {
        let t1 = new Triangle(5, 5, 5)
        expect(t1.isEquilateral()).to.equal(true)
        let t2 = new Triangle(6, 5, 5)
        expect(t2.isEquilateral()).to.equal(false)
        done();
    })
    it('9. Triangle isocèle', function(done) {
        let t1 = new Triangle(5, 5, 6)
        expect(t1.isIsocele()).to.equal(true)
        let t2 = new Triangle(6, 5, 7)
        expect(t2.isIsocele()).to.equal(false)
        done();
    })
    it('10. Triangle rectangle', function(done) {
        let t1 = new Triangle(3, 4, 5)
        expect(t1.isRectangle()).to.equal(true)
        let t2 = new Triangle(2, 4, 5)
        expect(t2.isRectangle()).to.equal(false)
        done();
    })
    it('11. Triangle scalene', function(done) {
        let t1 = new Triangle(8, 5, 6)
        expect(t1.isScalene()).to.equal(true)
        let t2 = new Triangle(5, 5, 6)
        expect(t2.isScalene()).to.equal(false)
        done();
    })
})

describe('Testing the Cercle Functions', function() {
    it('1. Cercle avec bon type de rayon', function(done){
        let c1 = new Cercle("5")
        expect(typeof c1.getRayon()).to.equal('undefined')
        let c2 = new Cercle(-1)
        expect(typeof c2.getRayon()).to.equal('undefined')
        let c3 = new Cercle(0)
        expect(typeof c3.getRayon()).to.equal('undefined')
        let c4 = new Cercle(2)
        expect(typeof c4.getRayon()).to.equal('number')
        done();
    })
    it('2. Le rayon d\'un cercle', function(done){
        let c1 = new Cercle(5)
        expect(c1.getRayon()).to.equal(5)
        done()
    })
    it('3. Le diamètre d\'un cercle', function(done){
        let c1 = new Cercle(5)
        expect(c1.getDiametre()).to.equal(10)
        done()
    })
    it('4. L\'aire d\'un cercle', function(done){
        let c1 = new Cercle(5)
        expect(c1.getAire()).to.equal(78.54)
        done()
    })

})