class Cube {
    constructor(length) {
        this.length = length;
    }
    
    getSideLength () {
        return this.length;
    }
    
    getSurfaceArea () {
        return (this.length * this.length) * 6;
    }
    
    getVolume () {
        return Math.pow(this.length,3);
    }
}

class Triangle {
    constructor(ab, ac, bc) {
        if (this.checkParams(ab, ac, bc) && this.isValid(ab, ac, bc)) {
            this.ab = ab;
            this.ac = ac;
            this.bc = bc;
        }
    }

    isEquilateral() {
        return this.ab === this.ac && this.ab === this.bc && this.bc === this.ac
    }

    isIsocele() {
        return !this.isEquilateral() && (this.ab === this.ac || this.ab === this.bc || this.bc === this.ac)
    }

    isScalene() {
        return this.ab !== this.bc && this.ab!== this.ac && this.bc !== this.bc !== this.ac;
    }

    isRectangle() {
        let tab = [this.ab, this.bc, this.ac]
        tab = tab.sort();
        let hyp = tab.pop();
        let a = tab[0]
        let b = tab[1]
        return Math.pow(hyp, 2) === (Math.pow(a, 2) + Math.pow(b, 2))
    }


    getSurface() {
        let p = this.getPerimetre() / 2
        let square_root = p *((p-this.ab) * (p - this.ac) * (p - this.bc))
        return parseFloat(Math.sqrt(square_root).toFixed(1))
    }

    getPerimetre() {
        return this.ab + this.ac + this.bc;
    }

    isValid(a, b, c){
        if (a === Math.max(a, b, c)){
            return this.isSumGreater(a, b, c);
        }
        else if (b === Math.max(a, b, c)){
            return this.isSumGreater(b, a, c)
        }
        else {
            return this.isSumGreater(c, a, b)
        }
    }

    isSumGreater(max, b, c) {
        return max < b + c;
    }

    checkParams(a, b, c) {
        if (this.checkParam(a) && this.checkParam(b) && this.checkParam(c)) {
            return true;
        }
    }

    checkParam(x) {
        if (typeof x === 'number' && x > 0){
            return true;
        }
    }

    getABLength() {
        return this.ab;
    }

    getACLength() {
        return this.ac;
    }

    getBCLength() {
        return this.bc;
    }
}

class Cercle {
    constructor(rayon) {
        if (typeof rayon === 'number' && rayon > 0){
            this.rayon = rayon
        }
    }

    getDiametre(){
        return this.rayon * 2
    }

    getRayon() {
        return this.rayon
    }

    getAire(){
        return parseFloat((Math.PI * Math.pow(this.rayon, 2)).toFixed(2))
    }

}

module.exports = {
    Cube:Cube,
    Triangle:Triangle,
    Cercle:Cercle
}